from django.contrib import admin

from .models import Blogger, BlogTag, Plan, PromoCode, User, BotSettings, BotMessages


# Register your models here.


class AdminPlan(admin.ModelAdmin):
    exclude = ["plan_id"]


class AdminUser(admin.ModelAdmin):
    exclude = ["plan_start", "chat_id", "user_id", "first_subscribtion"]


admin.site.register(Blogger)
admin.site.register(BlogTag)
admin.site.register(User, AdminUser)
admin.site.register(Plan, AdminPlan)
admin.site.register(PromoCode)
admin.site.register(BotSettings)
admin.site.register(BotMessages)
