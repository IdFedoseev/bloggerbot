from telegram import (
    InlineKeyboardMarkup,
    InlineKeyboardButton,
)

from application.models import Plan, Blogger
from bot import helpers

TSUPPORT = helpers.get_techsup_link()


def build_menu(buttons, n_cols, header_buttons=None, footer_buttons=None):
    menu = [buttons[i : i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu


def main_menu_keyboard(with_trial=True, with_subscribe=True):
    reply_keyboard = [
        [InlineKeyboardButton("Что такое блоггер бот?", callback_data="get_info")],
        [InlineKeyboardButton("Связаться с тех поддержкой", url=TSUPPORT)],
    ]
    if with_trial:
        reply_keyboard.insert(
            2,
            [
                InlineKeyboardButton(
                    "Бесплатный тестовый период", callback_data="test_period"
                )
            ],
        )
    if with_subscribe:
        reply_keyboard.insert(
            1,
            [InlineKeyboardButton("Оформить подписку", callback_data="subscribe")],
        )
    else:
        reply_keyboard.insert(
            1, [InlineKeyboardButton("Выбрать блоггера", callback_data="start_select")]
        )

    return InlineKeyboardMarkup(reply_keyboard)


def select_blogger_start():
    reply_keyboard = [
        [InlineKeyboardButton("по охватам в сториз", callback_data="stories")],
        [InlineKeyboardButton("по тематике блога", callback_data="blog_tag")],
        [
            InlineKeyboardButton(
                "по количеству подписчиков", callback_data="subscribers"
            )
        ],
        [InlineKeyboardButton("посмотреть всех", callback_data="see_all")],
        [InlineKeyboardButton("Вернуться в меню", callback_data="back_to_menu")],
    ]
    return InlineKeyboardMarkup(reply_keyboard)


def stories_diapason():
    reply_keyboard = [
        [InlineKeyboardButton("До 100", callback_data="stories_0_100")],
        [InlineKeyboardButton("100-300", callback_data="stories_100_300")],
        [InlineKeyboardButton("300-600", callback_data="stories_300_600")],
        [InlineKeyboardButton("600+", callback_data="stories_600_*")],
        [InlineKeyboardButton("Посмотреть всех", callback_data="stories_all")],
    ]
    return InlineKeyboardMarkup(reply_keyboard)


def subscribers_diapason():
    reply_keyboard = [
        [InlineKeyboardButton("До 1000", callback_data="subs_0_1000")],
        [InlineKeyboardButton("От 1000 до 3000", callback_data="subs_1000_3000")],
        [InlineKeyboardButton("От 3000 до 5000", callback_data="subs_3000_5000")],
        [InlineKeyboardButton("От 5000 до 10000", callback_data="subs_5000_10000")],
        [InlineKeyboardButton("От 10000", callback_data="subs_10000_*")],
        [InlineKeyboardButton("Посмотреть всех", callback_data="subs_all")],
    ]
    return InlineKeyboardMarkup(reply_keyboard)


def blog_tag_filter(tags: list):
    keyboard = []
    for tag in tags:
        keyboard.append(
            [InlineKeyboardButton(text=tag.tag, callback_data=f"tag_{tag.tag}")]
        )
    keyboard.append([InlineKeyboardButton("Посмотреть всех", callback_data="tag_all")])
    return InlineKeyboardMarkup(keyboard)


def plans_keyboard(plans: list):
    keyboard = []
    for plan in plans:
        keyboard.append(
            [InlineKeyboardButton(text=plan.name, callback_data=f"plan_{plan.plan_id}")]
        )
    return InlineKeyboardMarkup(keyboard)


def selected_plan(plan: Plan):
    keyboard = [
        [
            InlineKeyboardButton(
                text=f"Цена: {plan.price}",
                callback_data=f"createpayment_{plan.plan_id}",
            )
        ],
        [InlineKeyboardButton(text="Знаю промокод", callback_data=f"ask_promocode")],
    ]
    return InlineKeyboardMarkup(keyboard)


def tech_support():
    keyboard = [
        [InlineKeyboardButton("Связаться с тех поддержкой", url=TSUPPORT)],
    ]
    return InlineKeyboardMarkup(keyboard)


def notified_menu():
    keyboard = [
        [InlineKeyboardButton("Оформить подписку", callback_data="subscribe")],
        [InlineKeyboardButton("Вернуться в меню", callback_data="back_to_menu")],
    ]
    return InlineKeyboardMarkup(keyboard)


def blogger_link_keyboard(blogger: Blogger):
    keyboard = []
    if (
        blogger.messenger_link is not None
        and blogger.messenger_link != "-"
        and blogger.messenger_link != ""
    ):
        keyboard.append(
            [
                InlineKeyboardButton(
                    text="Ссылка на мессенджер", url=blogger.messenger_link
                )
            ]
        )
    if blogger.instagram_link is not None and blogger.instagram_link != "-":
        keyboard.append(
            [
                InlineKeyboardButton(
                    text="Ссылка на инстаграм", url=blogger.instagram_link
                )
            ]
        )
    if len(keyboard) == 0:
        return None
    return InlineKeyboardMarkup(keyboard)
