from django.core.management.base import BaseCommand

from bot import bot


class Command(BaseCommand):
    help = 'Just a command for launching a Telegram bot.'

    def handle(self, *args, **options):
        bot.main()
