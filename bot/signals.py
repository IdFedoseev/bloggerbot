from django.db.models.signals import post_save
from django.dispatch import receiver
from application.models import BotMessages, User


@receiver(post_save, sender=BotMessages)
def send_post_to_users(sender, instance, created, **kwargs):
    from bot.tasks import send_telegram_message
    """
    Обработчик сигнала для отправки сообщения бота всем пользователям при создании нового экземпляра BotMessage.

    Эта функция активируется сигналом post_save модели BotMessages.

    Импорт задачи send_telegram_message внутри функции, чтобы избежать потенциальных
    проблем циклического импорта. Если создается новый BotMessage, он планирует задачу send_telegram_message
    для асинхронного запуска с Celery.
    """
    if created:
        users = User.objects.all()
        for user in users:
            chat_id = user.chat_id
            message_text = instance.text
            message_image = instance.image
            if not message_image:
                data = {
                    "chat_id": chat_id,
                    "text": message_text,
                }
            else:
                data = {
                    "chat_id": chat_id,
                    "photo": message_image.path,
                    "text": message_text,
                }
            send_telegram_message.delay(data)
