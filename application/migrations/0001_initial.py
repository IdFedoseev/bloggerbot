# Generated by Django 5.0.6 on 2024-06-22 11:36

import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BlogTag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name': 'Тематика',
                'verbose_name_plural': 'Тематики',
                'db_table': 'blog_tag',
            },
        ),
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('plan_id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('term_in_days', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Тариф',
                'verbose_name_plural': 'Тарифы',
                'db_table': 'plans',
                'ordering': ['term_in_days'],
            },
        ),
        migrations.CreateModel(
            name='PromoCode',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=50)),
                ('only_for_registration', models.BooleanField(default=True)),
                ('percent_discount', models.IntegerField()),
                ('money_discount', models.DecimalField(decimal_places=2, max_digits=8)),
                ('free_term_in_days', models.IntegerField()),
                ('expired_at', models.DateTimeField()),
                ('max_activations', models.IntegerField()),
                ('active', models.BooleanField()),
            ],
            options={
                'verbose_name': 'Промокод',
                'verbose_name_plural': 'Промокоды',
                'db_table': 'promocodes',
                'ordering': ['active'],
            },
        ),
        migrations.CreateModel(
            name='Blogger',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='Имя')),
                ('nickname', models.CharField(max_length=30, verbose_name='Никнейм')),
                ('subscribers', models.IntegerField(verbose_name='Кол-во подписчиков')),
                ('stories_coverage_min', models.IntegerField(blank=True, default=None, verbose_name='Охват в сториз от')),
                ('stories_coverage_max', models.IntegerField(blank=True, default=None, verbose_name='Охват в сториз до')),
                ('post_coverage_min', models.IntegerField(blank=True, default=None, verbose_name='Охват постов от')),
                ('post_coverage_max', models.IntegerField(blank=True, default=None, verbose_name='Охват постов до')),
                ('reels_coverage_min', models.IntegerField(blank=True, default=None, verbose_name='Охват в рилс от')),
                ('reels_coverage_max', models.IntegerField(blank=True, default=None, verbose_name='Охват в рилс до')),
                ('barter', models.BooleanField(default=False, verbose_name='Бартер')),
                ('barter_terms', models.CharField(max_length=200, verbose_name='Условия бартера')),
                ('stop_factor_description', models.CharField(max_length=200, verbose_name='Не берет в рекламу')),
                ('experience', models.CharField(max_length=200, verbose_name='Опыт в рекламе')),
                ('messenger_link', models.CharField(blank=True, default=None, max_length=100, verbose_name='Ссылка на мессенджер')),
                ('instagram_link', models.CharField(blank=True, default=None, max_length=100, verbose_name='Ссылка на инстаграм')),
                ('tags', models.ManyToManyField(to='application.blogtag', verbose_name='Тематика блога')),
            ],
            options={
                'verbose_name': 'Блоггер',
                'verbose_name_plural': 'Блоггеры',
                'db_table': 'blogger',
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.AutoField(primary_key=True, serialize=False)),
                ('username', models.CharField(blank=True, max_length=255, null=True)),
                ('name', models.CharField(max_length=255)),
                ('chat_id', models.CharField(blank=True, max_length=255, null=True)),
                ('telegram_id', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('trial_available', models.BooleanField(default=True)),
                ('plan', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='application.plan')),
            ],
            options={
                'verbose_name': 'Пользователь',
                'verbose_name_plural': 'Пользователи',
                'db_table': 'users',
                'ordering': ['created_at'],
            },
        ),
    ]
