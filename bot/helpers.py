from datetime import date

from asgiref.sync import sync_to_async
from django.db.models import Q

from application.models import BlogTag, Blogger, Plan, PromoCode, User, BotSettings


def get_bot_settings() -> BotSettings:
    return BotSettings.objects.first()


@sync_to_async
def get_test_period_mesage():
    return (
        BotSettings.objects.values("test_period_message")
        .last()
        .get("test_period_message")
    )


@sync_to_async
def get_welcome_message():
    return BotSettings.objects.values("welcome_message").last().get("welcome_message")


def get_techsup_link():
    return BotSettings.objects.values("techsup_link").last().get("techsup_link")


@sync_to_async
def get_all_tags():
    tags_query = BlogTag.objects.all()
    tags = []
    for tag in tags_query:
        tags.append(tag)
    return tags


@sync_to_async
def get_bot_info():
    return BotSettings.objects.values("info_message").last().get("info_message")


@sync_to_async
def find_promocode(code: str, allow_welcome_promocodes=True) -> PromoCode:
    if allow_welcome_promocodes:
        promocode = (
            PromoCode.objects.filter(value=code)
            .filter(active=True)
            .filter(Q(expired_at__gt=date.today()))
            .filter(Q(max_activations__gt=0) | Q(max_activations=-1))
        )
    else:
        promocode = (
            PromoCode.objects.filter(value=code)
            .filter(active=True)
            .filter(expired_at__gt=date.today())
            .filter(Q(max_activations__gt=0) | Q(max_activations=-1))
            .filter(only_for_registration=False)
        )
    return promocode.first()


@sync_to_async
def find_user(user_id) -> User:
    user = User.objects.filter(user_id=user_id).first()
    return user


@sync_to_async
def get_user_plan(user: User) -> Plan:
    return user.plan


@sync_to_async
def select_plans():
    plan_query = Plan.objects.filter(price__gt=0).all()
    plans = [plan for plan in plan_query]
    return plans


@sync_to_async
def get_plan(id: int):
    plan = Plan.objects.filter(plan_id=id).first()
    return plan


@sync_to_async
def select_bloggers(filter_name: str, filter_value: list, limited: bool = False):
    blogger_query = Blogger.objects

    match filter_name:
        case "stories":
            if filter_value[0] != "all":
                stories_from = int(filter_value[0])
                if stories_from > 0:
                    blogger_query = blogger_query.filter(
                        stories_coverage_min__gte=stories_from
                    )
                if filter_value[1] != "*":
                    blogger_query = blogger_query.filter(
                        stories_coverage_max__lte=filter_value[1]
                    )
        case "subs":
            if filter_value[0] != "all":
                subs_from = int(filter_value[0])
                if subs_from > 0:
                    blogger_query = blogger_query.filter(subscribers__gte=subs_from)
                if filter_value[1] != "*":
                    blogger_query = blogger_query.filter(
                        subscribers__lte=filter_value[1]
                    )
        case "tag":
            if filter_value[0] != "all":
                blogger_query = blogger_query.filter(tags__tag__in=filter_value)
        case "all":
            pass

    if limited:
        bloggers = [blogger for blogger in blogger_query.all()[:5]]
    else:
        bloggers = [blogger for blogger in blogger_query.all()]
    return bloggers


@sync_to_async
def select_bloggers_old(
    stories_filter: list, subscribers_filter: list, tags_filter: list
):
    blogger_query = Blogger.objects
    if stories_filter[0] != "all":
        stories_from = int(stories_filter[0])
        if stories_from > 0:
            blogger_query = blogger_query.filter(stories_coverage_min__gte=stories_from)
        if stories_filter[1] != 0:
            blogger_query = blogger_query.filter(
                stories_coverage_max__lte=stories_filter[1]
            )

    if subscribers_filter[0] != "all":
        subscribers_from = int(subscribers_filter[0])
        if subscribers_from > 0:
            blogger_query = blogger_query.filter(subscribers__gte=subscribers_from)
        if subscribers_filter[1] != 0:
            blogger_query = blogger_query.filter(subscribers__lte=subscribers_filter[1])

    if tags_filter[0] != "all":
        blogger_query = blogger_query.filter(tags__tag__in=tags_filter)

    bloggers = [blogger for blogger in blogger_query.all()]
    return bloggers


@sync_to_async
def form_blogger_card(blogger: Blogger):
    tags_string = ""
    tags = blogger.tags.all()

    for tag in tags:
        tags_string += tag.tag + ","
    return f"""
Имя: {blogger.name}
Никнейм: {blogger.nickname}
    
Количество подписчиков: {blogger.subscribers}
Вилка охватов в сторис: {blogger.stories_coverage_min}-{blogger.stories_coverage_max}
Охват постов: {blogger.post_coverage}
Охват рилс: {blogger.reels_coverage}
Самый высокий охват в сториз за последние 3 месяца: f{blogger.last_stories_record_3m}
Регулярность: f{blogger.regularity}
Бартер: {"Есть" if blogger.barter else "Нет"}
Условия: {blogger.barter_terms}
Тематика блога: {tags_string}
Не берет в рекламу: {blogger.stop_factor_description}
Опыт в рекламе: {blogger.experience}
Дополнительный комментарий: {blogger.additional_comments}
Инстаграм: {blogger.instagram_link}
Мессенджер: {blogger.messenger_link}
    """
