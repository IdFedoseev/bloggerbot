from django.http import HttpResponse, HttpResponseNotFound
from django.core.cache import cache
from django.conf import settings
from django.shortcuts import render
from django.test import RequestFactory
from django.views.decorators.csrf import csrf_exempt
import json
