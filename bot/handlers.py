import logging
from datetime import datetime
from os import getenv

from asgiref.sync import sync_to_async
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.error import BadRequest
from telegram.ext import ContextTypes, ConversationHandler

from application.models import User, Plan
from bot import helpers
from bot import keyboards

GREETING, MAIN_MENU, SUBSCRIPTION, PROMOCODE, SELECT_BLOGGER = range(5)


def get_next_filter(update: Update, context: ContextTypes.DEFAULT_TYPE):
    filters = ("subscribers", "tags", "stories")
    applyed_filters = context.user_data.get("applyed_filters")

    for filter in filters:
        if filter not in applyed_filters:
            return filter

    return "no match"


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await helpers.find_user(update.effective_user.id)
    welcome_message = await helpers.get_welcome_message()
    if user is None:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=welcome_message,
        )
        return GREETING
    plan = await helpers.get_user_plan(user)
    with_subsctibe = True
    if plan != None:
        with_subsctibe = False
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"{user.name}, очень приятно! Вот что я могу тебе предложить:",
        reply_markup=keyboards.main_menu_keyboard(
            user.trial_available, with_subscribe=with_subsctibe
        ),
    )
    return MAIN_MENU


async def get_info(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await sync_to_async(
        User.objects.filter(user_id=update.effective_user.id).first
    )()
    plan = await helpers.get_user_plan(user)
    keyboard = [
        [InlineKeyboardButton("Главное меню", callback_data="main_menu")],
    ]
    if plan is None:
        keyboard.insert(
            0, [InlineKeyboardButton("Оформить подписку", callback_data="subscribe")]
        )
    if user.trial_available:
        keyboard.insert(
            1,
            [
                InlineKeyboardButton(
                    "Бесплатный тестовый период", callback_data="test_period"
                )
            ],
        )
    keyboard_markup = InlineKeyboardMarkup(keyboard)
    info_text = await helpers.get_bot_info()
    await context.bot.edit_message_text(
        text=info_text,
        message_id=update.effective_message.id,
        chat_id=update.effective_chat.id,
    )
    await context.bot.edit_message_reply_markup(
        chat_id=update.effective_chat.id,
        message_id=update.effective_message.id,
        reply_markup=keyboard_markup,
    )
    return SUBSCRIPTION


async def back_to_menu(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await sync_to_async(
        User.objects.filter(user_id=update.effective_user.id).first
    )()
    plan = await helpers.get_user_plan(user)
    with_subscribe = True
    if plan is not None:
        with_subscribe = False
    await context.bot.edit_message_reply_markup(
        chat_id=update.effective_chat.id,
        message_id=update.effective_message.id,
        reply_markup=keyboards.main_menu_keyboard(
            user.trial_available, with_subscribe=with_subscribe
        ),
    )
    return MAIN_MENU


async def start_blogger_select(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Выбрать блоггера",
        reply_markup=keyboards.select_blogger_start(),
    )
    return SELECT_BLOGGER


async def get_subscription(update: Update, context: ContextTypes.DEFAULT_TYPE):
    plans = await helpers.select_plans()
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Оформление подписки",
        reply_markup=keyboards.plans_keyboard(plans),
    )
    return SUBSCRIPTION


async def subscribe(update: Update, context: ContextTypes.DEFAULT_TYPE):
    plan_message = update.callback_query.data
    plan_id = int(plan_message.split("_")[1:][0])
    plan = await helpers.get_plan(plan_id)

    context.user_data["plan"] = plan
    context.user_data["price"] = plan.price

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=plan.name,
        reply_markup=keyboards.selected_plan(plan),
    )


async def payment(update: Update, context: ContextTypes.DEFAULT_TYPE):
    plan = context.user_data.get("plan")
    price = int(context.user_data.get("price")) * 100
    await context.bot.send_invoice(
        chat_id=update.effective_chat.id,
        title="Платеж",
        description="Оплата подписки",
        provider_token=getenv("PAYMENT_PROVIDER_TOKEN"),
        currency="RUB",
        prices=[{"label": "руб.", "amount": price}],
        payload=plan.name,
    )


async def precheckout_callback(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> None:
    """Answers the PreQecheckoutQuery"""
    plan = context.user_data.get("plan")
    query = update.pre_checkout_query
    user = await helpers.find_user(query.from_user.id)
    user.plan = plan
    user.notified = False
    user.first_subscribtion = False
    await sync_to_async(user.save)()
    # TODO: Проверять платеж
    await context.bot.answer_pre_checkout_query(query.id, ok=True)
    return SELECT_BLOGGER


async def success_payment(update: Update, context: ContextTypes.DEFAULT_TYPE):
    plan = context.user_data.get("plan")
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text=f"{plan.name} оформлена"
    )
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Выбрать блоггера",
        reply_markup=keyboards.select_blogger_start(),
    )
    return SELECT_BLOGGER


async def ask_promocode(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text="Введите ваш промокод"
    )
    return PROMOCODE


async def apply_promocode(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await sync_to_async(
        User.objects.filter(user_id=update.effective_user.id).first
    )()
    promocode_text = update.effective_message.text
    if user.first_subscribtion:
        promocode = await helpers.find_promocode(promocode_text)
    else:
        promocode = await helpers.find_promocode(promocode_text, False)
    if promocode is None:
        keyboard = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        text="Перейти к оплате", callback_data="createpayment_*"
                    )
                ]
            ]
        )
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Такого промокода нет, попробуйте другой",
            reply_markup=keyboard,
        )
        return PROMOCODE
    plan = context.user_data.get("plan")
    if promocode.percent_discount is not None:
        discount_amount = (plan.price / 100) * promocode.percent_discount
        price = plan.price - discount_amount
    elif promocode.money_discount is not None:
        if promocode.money_discount >= plan.price:
            price = 0
        if promocode.money_discount < plan.price:
            price = plan.price - promocode.money_discount

    context.user_data["price"] = price
    if not promocode.is_infinite():
        promocode.max_activations -= 1
        await sync_to_async(promocode.save)()
    keyboard = [
        [
            InlineKeyboardButton(
                text=f"Оплатить {price}", callback_data="createpayment_*"
            )
        ]
    ]

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Условия рассчитаны",
        reply_markup=InlineKeyboardMarkup(keyboard),
    )
    return SUBSCRIPTION


async def get_name(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await sync_to_async(
        User.objects.filter(user_id=update.effective_user.id).first
    )()
    if user is None:
        user = User()
        user.user_id = update.effective_message.from_user.id
        user.chat_id = update.effective_chat.id
        user.username = update.effective_message.from_user.username
        user.name = update.effective_message.text
        user.created_at = datetime.now()
        user.updated_at = datetime.now()
    await sync_to_async(user.save)()
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"{user.name}, очень приятно! Вот что я могу тебе предложить:",
        reply_markup=keyboards.main_menu_keyboard(),
    )
    return MAIN_MENU


async def get_test_period(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user = await sync_to_async(
        User.objects.filter(user_id=update.effective_user.id).first
    )()
    if not user.trial_available:
        await context.bot.send_message(
            update.effective_chat.id,
            text="Тестовый период можно форомить только один раз",
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton("Главное меню", callback_data="main_menu")]]
            ),
        )
        return MAIN_MENU
    user.trial_available = False
    user.plan = await sync_to_async(Plan.objects.filter(plan_id=1).first)()
    user.plan_start = datetime.now()
    await sync_to_async(user.save)()
    txt = await helpers.get_test_period_mesage()
    await context.bot.send_message(chat_id=update.effective_chat.id, text=txt)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Выбрать блоггера",
        reply_markup=keyboards.select_blogger_start(),
    )
    return SELECT_BLOGGER


async def blogger_stories_filters(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        update.effective_chat.id,
        "По охватам в сторис:",
        reply_markup=keyboards.stories_diapason(),
    )
    return SELECT_BLOGGER


async def blogger_tag_filter(update: Update, context: ContextTypes.DEFAULT_TYPE):
    tags = await helpers.get_all_tags()
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="По тематике блога",
        reply_markup=keyboards.blog_tag_filter(tags=tags),
    )
    return SELECT_BLOGGER


async def blogger_subscribers_filter(
    update: Update, context: ContextTypes.DEFAULT_TYPE
):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="По количеству подписчиков",
        reply_markup=keyboards.subscribers_diapason(),
    )
    return SELECT_BLOGGER


async def blogger_by_stories(update: Update, context: ContextTypes.DEFAULT_TYPE):
    context.user_data["stories_filter"] = update.callback_query.data
    applyed_filters = context.user_data.get("applyed_filters")
    if applyed_filters is None:
        applyed_filters = []
    applyed_filters.append("stories")
    context.user_data["applyed_filters"] = applyed_filters
    await select_bloggers(update, context)
    return SELECT_BLOGGER


async def blogger_by_tag(update: Update, context: ContextTypes.DEFAULT_TYPE):
    context.user_data["tag_filter"] = update.callback_query.data
    context.user_data["applyed_filter"] = "tag"
    await select_bloggers(update, context)
    return SELECT_BLOGGER


async def blogger_by_subscribers(update: Update, context: ContextTypes.DEFAULT_TYPE):
    context.user_data["subscribers_filter"] = update.callback_query.data
    applyed_filters = context.user_data.get("applyed_filters")
    if applyed_filters is None:
        applyed_filters = []
    applyed_filters.append("subscribers")
    context.user_data["applyed_filters"] = applyed_filters
    await select_bloggers(update, context)
    return SELECT_BLOGGER


async def select_bloggers(update: Update, context: ContextTypes.DEFAULT_TYPE):
    filter_name = update.callback_query.data.split("_")[0]
    filter_value = update.callback_query.data.split("_")[1:]
    limited = False
    user = await helpers.find_user(user_id=update.effective_user.id)
    user_plan = await helpers.get_user_plan(user)
    if user_plan.is_trial():
        limited = True
    bloggers = await helpers.select_bloggers(
        filter_name=filter_name, filter_value=filter_value, limited=limited
    )
    for blogger in bloggers:
        try:
            keyboard = keyboards.blogger_link_keyboard(blogger)
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=await helpers.form_blogger_card(blogger),
                reply_markup=keyboard,
            )
        except BadRequest:
            logging.log(logging.ERROR, blogger.nickname)
    await context.bot.send_message(
        text="Выбрать блоггера",
        chat_id=update.effective_chat.id,
        reply_markup=keyboards.select_blogger_start(),
    )
    return SELECT_BLOGGER


async def display_all_bloggers(update: Update, context: ContextTypes.DEFAULT_TYPE):
    bloggers = await helpers.select_bloggers(
        filter_name="all",
        filter_value=["all"],
    )
    card = ""
    for index, blogger in enumerate(bloggers):
        try:
            # print(index)
            # if index + 1 % 80 == 0:
            #     time.sleep(60)
            keyboard = keyboards.blogger_link_keyboard(blogger)
            if index % 2 != 0 or index == 0:
                card += (
                    await helpers.form_blogger_card(blogger)
                    + "\n\n================================"
                )
                continue
            else:
                await context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=card,
                )
                card = ""
                card += (
                    await helpers.form_blogger_card(blogger)
                    + "\n\n================================"
                )

        except BadRequest:
            logging.log(
                logging.ERROR, f"invalid messenger/insta link for blogger {blogger}"
            )
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=card,
    )
    await context.bot.send_message(
        text="Выбрать блоггера",
        chat_id=update.effective_chat.id,
        reply_markup=keyboards.select_blogger_start(),
    )
    return SELECT_BLOGGER


async def done(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Вывод собранной информации и завершение разговора."""
    user_data = context.user_data
    return ConversationHandler.END
