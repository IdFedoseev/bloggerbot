# Generated by Django 5.0.6 on 2024-06-24 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0004_user_notified'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promocode',
            name='expired_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='promocode',
            name='free_term_in_days',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='promocode',
            name='max_activations',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='promocode',
            name='money_discount',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True),
        ),
        migrations.AlterField(
            model_name='promocode',
            name='percent_discount',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
