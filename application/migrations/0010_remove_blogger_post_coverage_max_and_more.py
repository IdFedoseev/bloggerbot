# Generated by Django 5.0.7 on 2024-07-19 09:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0009_alter_botsettings_options'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blogger',
            name='post_coverage_max',
        ),
        migrations.RemoveField(
            model_name='blogger',
            name='post_coverage_min',
        ),
        migrations.RemoveField(
            model_name='blogger',
            name='reels_coverage_max',
        ),
        migrations.RemoveField(
            model_name='blogger',
            name='reels_coverage_min',
        ),
        migrations.AddField(
            model_name='blogger',
            name='last_stories_record_3m',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Самый высокий охват в сториз за последние 3 месяца'),
        ),
        migrations.AddField(
            model_name='blogger',
            name='post_coverage',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Охват постов средний'),
        ),
        migrations.AddField(
            model_name='blogger',
            name='reels_coverage',
            field=models.IntegerField(blank=True, default=None, null=True, verbose_name='Охват в рилс средний'),
        ),
        migrations.AddField(
            model_name='blogger',
            name='regularity',
            field=models.CharField(blank=True, default=None, max_length=200, null=True, verbose_name='Регулярность'),
        ),
        migrations.AlterField(
            model_name='blogger',
            name='barter_terms',
            field=models.CharField(max_length=200, verbose_name='Условия рекламы'),
        ),
    ]
