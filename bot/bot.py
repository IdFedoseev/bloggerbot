import logging
from telegram import Update
from telegram.ext import (
    ApplicationBuilder,
    CommandHandler,
    MessageHandler,
    ConversationHandler,
    filters,
    CallbackQueryHandler,
    PreCheckoutQueryHandler,
)
from telegram import ReplyKeyboardMarkup

from bot import handlers
from django.conf import settings as django_settings
from bloggerbot import settings

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)


def main():
    application = ApplicationBuilder().token(settings.TELEGRAM_TOKEN).build()

    conv_handler = ConversationHandler(
        allow_reentry=True,
        entry_points=[
            CommandHandler("start", handlers.start),
            CallbackQueryHandler(
                pattern="back_to_menu", callback=handlers.back_to_menu
            ),
            CallbackQueryHandler(
                pattern="^subscribe$",
                callback=handlers.get_subscription,
            ),
        ],
        states={
            handlers.GREETING: [
                MessageHandler(filters=filters.TEXT, callback=handlers.get_name)
            ],
            handlers.MAIN_MENU: [
                CallbackQueryHandler(callback=handlers.get_info, pattern="get_info"),
                CallbackQueryHandler(
                    callback=handlers.get_subscription, pattern="^subscribe$"
                ),
                CallbackQueryHandler(
                    callback=handlers.get_test_period, pattern="test_period"
                ),
                CallbackQueryHandler(
                    callback=handlers.back_to_menu, pattern="^main_menu$"
                ),
                CallbackQueryHandler(callback=handlers.subscribe, pattern="^plan_*"),
                MessageHandler(
                    filters=filters.SUCCESSFUL_PAYMENT,
                    callback=handlers.success_payment,
                ),
                CallbackQueryHandler(callback=handlers.start_blogger_select, pattern="^start_select$"),
            ],
            handlers.SUBSCRIPTION: [
                CallbackQueryHandler(
                    callback=handlers.get_subscription, pattern="^subscribe$"
                ),
                CallbackQueryHandler(
                    callback=handlers.back_to_menu, pattern="main_menu"
                ),
                CallbackQueryHandler(
                    callback=handlers.get_test_period, pattern="test_period"
                ),
                CallbackQueryHandler(callback=handlers.subscribe, pattern="plan_*"),
                CallbackQueryHandler(
                    callback=handlers.payment, pattern="createpayment_*"
                ),
                CallbackQueryHandler(
                    callback=handlers.ask_promocode, pattern="^ask_promocode$"
                ),
                MessageHandler(
                    filters=filters.SUCCESSFUL_PAYMENT,
                    callback=handlers.success_payment,
                ),
            ],
            handlers.PROMOCODE: [
                MessageHandler(filters=filters.TEXT, callback=handlers.apply_promocode),
                MessageHandler(
                    filters=filters.SUCCESSFUL_PAYMENT,
                    callback=handlers.success_payment,
                ),
                CallbackQueryHandler(
                    callback=handlers.payment, pattern="createpayment_*"
                ),
            ],
            handlers.SELECT_BLOGGER: [
                CallbackQueryHandler(
                    callback=handlers.blogger_stories_filters,
                    pattern="^stories$",
                ),
                CallbackQueryHandler(
                    callback=handlers.blogger_tag_filter, pattern="^blog_tag$"
                ),
                CallbackQueryHandler(
                    callback=handlers.blogger_subscribers_filter,
                    pattern="^subscribers$",
                ),
                CallbackQueryHandler(
                    callback=handlers.select_bloggers, pattern="stories_*"
                ),
                CallbackQueryHandler(callback=handlers.select_bloggers, pattern="tag_*"),
                CallbackQueryHandler(
                    callback=handlers.select_bloggers, pattern="subs_*"
                ),
                CallbackQueryHandler(
                    callback=handlers.display_all_bloggers, pattern="^see_all$"
                ),
                CallbackQueryHandler(
                    callback=handlers.get_subscription, pattern="^subscribe$"
                ),
                CallbackQueryHandler(
                    callback=handlers.back_to_menu, pattern="^back_menu$"
                ),
                MessageHandler(
                    filters=filters.SUCCESSFUL_PAYMENT,
                    callback=handlers.success_payment,
                ),
            ],
        },
        fallbacks=[MessageHandler(filters.Regex("^Done$"), handlers.done)],
    )

    application.add_handler(conv_handler)
    application.add_handler(PreCheckoutQueryHandler(handlers.precheckout_callback))

    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()

    """
    TODO: Сделать публикацию постов
    TODO: Категория (добавленные за последние 14 дней)
    """
