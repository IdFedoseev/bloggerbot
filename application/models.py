from django.db import models
from django.utils import timezone


class BlogTag(models.Model):
    tag = models.CharField(max_length=30)

    def __str__(self) -> str:
        return str(self.tag)

    class Meta:
        db_table = "blog_tag"
        verbose_name = "Тематика"
        verbose_name_plural = "Тематики"


class Blogger(models.Model):
    name = models.CharField(max_length=30, verbose_name="Имя")
    nickname = models.CharField(max_length=30, verbose_name="Никнейм")
    subscribers = models.IntegerField(verbose_name="Кол-во подписчиков")
    last_stories_record_3m = models.IntegerField(
        default=None,
        blank=True,
        null=True,
        verbose_name="Самый высокий охват в сториз за последние 3 месяца",
    )
    stories_coverage_min = models.IntegerField(
        default=None, blank=True, verbose_name="Охват в сториз от"
    )
    stories_coverage_max = models.IntegerField(
        default=None, blank=True, verbose_name="Охват в сториз до"
    )
    post_coverage = models.IntegerField(
        default=None, blank=True, null=True, verbose_name="Охват постов средний"
    )
    reels_coverage = models.IntegerField(
        default=None, blank=True, null=True, verbose_name="Охват в рилс средний"
    )

    regularity = models.CharField(
        max_length=200, default=None, blank=True, null=True, verbose_name="Регулярность"
    )
    barter = models.BooleanField(default=False, verbose_name="Бартер")
    barter_terms = models.CharField(max_length=200, verbose_name="Условия рекламы")
    tags = models.ManyToManyField(BlogTag, verbose_name="Тематика блога")
    stop_factor_description = models.CharField(
        max_length=200, verbose_name="Не берет в рекламу"
    )
    experience = models.TextField(verbose_name="Опыт в рекламе")
    messenger_link = models.CharField(
        max_length=100, default=None, blank=True, verbose_name="Ссылка на мессенджер"
    )
    instagram_link = models.CharField(
        max_length=100, default=None, blank=True, verbose_name="Ссылка на инстаграм"
    )
    additional_comments = models.TextField(
        null=True, blank=True, verbose_name="Дополнительный комментарий"
    )

    def __str__(self) -> str:
        return f"{self.nickname} - {self.name}"

    class Meta:
        db_table = "blogger"
        verbose_name = "Блоггер"
        verbose_name_plural = "Блоггеры"


class Plan(models.Model):
    plan_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=2)
    term_in_days = models.IntegerField()

    def __str__(self):
        return self.name

    def is_trial(self) -> bool:
        return self.plan_id == 1

    class Meta:
        db_table = "plans"
        verbose_name = "Тариф"
        verbose_name_plural = "Тарифы"
        ordering = ["term_in_days"]


class User(models.Model):
    user_id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255)
    chat_id = models.CharField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)
    plan = models.ForeignKey(Plan, on_delete=models.SET_NULL, blank=True, null=True)
    plan_start = models.DateTimeField(default=None, blank=True, null=True)
    trial_available = models.BooleanField(
        default=True, verbose_name="Тестовый период доступен"
    )
    notified = models.BooleanField(default=False)
    first_subscribtion = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name} ({self.username})"

    class Meta:
        db_table = "users"
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
        ordering = ["created_at"]


class PromoCode(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name="Название")
    value = models.CharField(max_length=50, verbose_name="Промокод")
    description = models.CharField(max_length=50, verbose_name="Описание")
    only_for_registration = models.BooleanField(default=True)
    percent_discount = models.IntegerField(
        blank=True, null=True, verbose_name="Скидка в процентах"
    )
    money_discount = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name="Скидка в деньгах",
    )
    expired_at = models.DateField(
        blank=True, null=True, verbose_name="Дата окончания действия"
    )
    max_activations = models.IntegerField(
        verbose_name="Максимальное количество активаций (-1 для неограниченного действия)",
        default=100,
    )
    active = models.BooleanField(verbose_name="Активен")

    def __str__(self):
        return self.name

    def is_infinite(self) -> bool:
        return self.expired_at == -1

    class Meta:
        db_table = "promocodes"
        verbose_name = "Промокод"
        verbose_name_plural = "Промокоды"
        ordering = ["active"]


class BotSettings(models.Model):
    welcome_message = models.TextField(verbose_name="Приветственное сообщение")
    techsup_link = models.CharField(max_length=100, default="blank")
    info_message = models.TextField(verbose_name="Инфо о боте", default="Инфо о боте")
    test_period_message = models.TextField(
        verbose_name="Инфо о тестовом периоде", default="Тестовый период на 24ч."
    )
    pre_end_test_message = models.TextField(
        verbose_name="Сообщение за час до окончания тестового периода",
        default="До окончания тестового периода остался 1 час",
    )
    end_test_message = models.TextField(
        verbose_name="Сообщение конца тестового периода",
        default="Тестовый период окончен",
    )

    def __str__(self) -> str:
        return f"Настройка бота - {self.id}"

    class Meta:
        verbose_name = "Настройки бота"
        verbose_name_plural = "Настройки бота"


class BotMessages(models.Model):
    title = models.CharField(max_length=50, verbose_name="Заголовок сообщения")
    text = models.TextField(verbose_name="Текст сообщения", max_length=4000)
    image = models.ImageField(upload_to="posts/", blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Сообщение бота"
        verbose_name_plural = "Сообщения бота"
