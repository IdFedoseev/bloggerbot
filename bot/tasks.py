import datetime

from asgiref.sync import async_to_sync
from celery import shared_task
from telegram import Bot

from application.models import User
from bloggerbot import settings
from bot import handlers
from bot import helpers
from bot.keyboards import tech_support, notified_menu


@shared_task
def check_subscription():
    """
    берем всех пользователей и чекаем их подписку. Если подписка закончилась, отправляем сообщени
    """
    users = User.objects.filter(notified=False).filter(plan__isnull=False).all()
    bot = Bot(token=settings.TELEGRAM_TOKEN)
    for user in users:
        plan = user.plan
        plan_end_delta = datetime.timedelta(days=plan.term_in_days, hours=0, seconds=0)
        plan_end_timestamp = (user.plan_start + plan_end_delta).timestamp()
        now_timestamp = datetime.datetime.now().timestamp()

        bot_settings = helpers.get_bot_settings()

        if (now_timestamp - plan_end_timestamp) >= 3600:
            user.notified = True
            user.plan = None
            user.plan_start = None
            user.save()
            notified_text = bot_settings.end_test_message
            async_to_sync(bot.send_message)(
                chat_id=user.chat_id, text=notified_text, reply_markup=notified_menu()
            )
            return handlers.MAIN_MENU

        if (plan_end_timestamp - now_timestamp) <= 3600:
            txt = bot_settings.pre_end_test_message
            async_to_sync(bot.send_message)(
                chat_id=user.chat_id,
                text=txt,
                reply_markup=tech_support(),
            )
            return


@shared_task
def send_telegram_message(message: dict):
    """
    Асинхронная задача по отправке сообщения или фотографии всем пользователям через Telegram.
    """
    bot = Bot(token=settings.TELEGRAM_TOKEN)

    try:
        chat_id = message.get("chat_id")
        message_text = message.get("text")
        message_image = message.get("photo", None)
        if message_image:
            async_to_sync(bot.send_photo)(
                chat_id=chat_id, caption=message_text, photo=message_image
            )
        else:
            async_to_sync(bot.send_message)(
                chat_id=chat_id,
                text=message_text,
            )
    except Exception as e:
        print(f"Failed to send message to: {e}")
