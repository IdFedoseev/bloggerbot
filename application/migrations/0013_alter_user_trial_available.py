# Generated by Django 5.0.6 on 2024-07-23 03:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0012_botsettings_info_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='trial_available',
            field=models.BooleanField(default=True, verbose_name='Тестовый период доступен'),
        ),
    ]
